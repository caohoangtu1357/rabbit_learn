<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\TestQueue;
use App\Jobs\TestHighQueue;
use Illuminate\Bus\Batch;
use stdClass;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        dispatch(new TestQueue((object)["id"=>1,"name"=>"name 1"]))->onQueue('test-queue');
        dispatch(new TestQueue((object)["id"=>2,"name"=>"name 2"]))->onQueue('test-queue');
        dispatch(new TestHighQueue((object)["id"=>3,"name"=>"name 3"]))->onQueue('high-queue');
        dispatch(new TestQueue((object)["id"=>4,"name"=>"name 4"]))->onQueue('test-queue');
        return 0;
    }
}
